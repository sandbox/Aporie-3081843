<?php

namespace Drupal\multilingual_ux\Form;

use Drupal\node\NodeForm;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\multilingual_ux\Services\MultilingualUxFormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the node multilingual ux edit forms.
 *
 * @internal
 */
class MultilingualUxNodeForm extends NodeForm {

  /**
   * Constructs a NodeForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The factory for the temp store object.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, PrivateTempStoreFactory $temp_store_factory, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, AccountInterface $current_user, DateFormatterInterface $date_formatter, MultilingualUxFormBuilder $mux_form_builder) {
    parent::__construct($entity_repository, $temp_store_factory, $entity_type_bundle_info, $time, $current_user, $date_formatter);
    $this->muxFormBuilder = $mux_form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('tempstore.private'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('multilingual_ux.form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    // Remove langcode dropdown list.
    if (isset($form['langcode'])) {
      unset($form['langcode']);
    }
    $translations = $this->muxFormBuilder->getTranslatedStorageValues($this->entity);
    $sliced_form = $this->muxFormBuilder->sliceForm($form, $this->entity);
    // Reset the form so we can set our structure.
    $form_revamped = $sliced_form['form_elements'] + $sliced_form['all_languages'];
    $form_revamped['advanced'] = $form['advanced'];

    foreach ($translations['values'] as $langcode => $values) {
      $form_revamped['form_translations'][$langcode] = [
        '#type' => 'details',
        '#title' => $this->t('@Language translation', [
          '@Language' => $this->entity->getTranslationLanguages()[$langcode]->getName(),
        ]),
      ];
      $form_revamped['form_translations'][$langcode]['advanced_'.$langcode] = [
        '#type' => 'vertical_tabs',
        '#weight' => 99,
        '#attributes' => ['class' => ['entity-'.$langcode.'-meta']],
      ];

      $reordered_groups = $this->muxFormBuilder->reorderGroups($sliced_form, $langcode);
      $form_revamped['form_translations'][$langcode]['groups'] = $this->muxFormBuilder->localizeFieldGroups($reordered_groups['translatable'], $langcode, TRUE);

      $localized_fields = $this->muxFormBuilder->localizeFieldGroups($sliced_form['translatable'], $langcode);
      $form_revamped['form_translations'][$langcode]['fields_'.$langcode] = $this->muxFormBuilder->setLocalizedDefaultValues($localized_fields, $translations['values'][$langcode]);
      $form_revamped['form_translations'][$langcode]['fields_'.$langcode]['#tree'] = TRUE;
    }

    foreach ($reordered_groups['all_languages'] as $group_name => $group) {
      $form_revamped[$group_name] = $group;
    }
    //ksm($form_revamped['form_translations']['en']['fields_en']['field_taxonomy_term']);
    return $form_revamped;
  }

  /**
   * {@inheritdoc}
   *
   * @see
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Remove button and internal Form API values from submitted values.
    $form_state->cleanValues();
    // get translations values.
    $translation_values = $this->getTranslatedFormValues($form_state);
    ksm($translation_values['fields_en'], $this->muxFormBuilder->reduceArray($translation_values['fields_en']));
    // Retrieve all language values.
    $all_language_values = $this->muxFormBuilder->reduceArray(array_diff_key($form_state->getValues(), $translation_values));
    $all_language_values_to_ignore = ['created', 'meta', 'uid'];
    // Apply values to entities.
    $langcodes = $this->muxFormBuilder->getLangcodes($this->entity);
    foreach ($langcodes as $langcode) {
      // load translations.
      $node = $this->entity->getTranslation($langcode);
      // All language values.
      $node->setChangedTime($this->time->getRequestTime());
      $node->setOwnerId($all_language_values['uid']['value']);
      foreach ($all_language_values as $field_name => $values) {
        if (!in_array($field_name, $all_language_values_to_ignore)) {
          $node->{$field_name} = $values;
        }
      }
      // Translation values.
      // We don't want to override changed and let drupal set current time.
      // Menu is handled by multilingual_ux_menu_ui_form_node_form_submit callback.
      // Retranslate is handle by content_translation.
      // @TODO the path alias system is still super buggy.
      // It'll need to be fixed. @see #3007661.
      $translations_values_to_ignore = ['changed', 'menu', 'content_translation'];
      foreach ($this->muxFormBuilder->reduceArray($translation_values['fields_'.$langcode]) as $field_name => $values) {
        if (isset($values['target_id']['entity'])) {
          $values['target_id']['entity']->save();
          $values['target_id'] = $values['target_id']['entity']->id();
        }
        if (!in_array($field_name, $translations_values_to_ignore)) {
          $node->{$field_name} = $values;
        }
        elseif ($field_name === 'retranslate' && $values != FALSE) {
          // @TODO
        }
      }
      $node->save();
    }
  }

  /**
   * Get translatable fields values.
   *
   * @param array $form_state
   *  The form_state.
   */
  private function getTranslatedFormValues($form_state) {
    $values = $form_state->getValues();
    foreach ($values as $name => $value) {
      if (strpos($name, 'fields') !== FALSE) {
        $fields[$name] = $value;
      }
    }

    return $fields;
  }

}
