<?php

namespace Drupal\multilingual_ux\Services;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form Manager class.
 */
class MultilingualUxFormBuilder {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *  The messenger service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *  The entity type manager service.
   */
  public function __construct(MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
}

  /**
   * Get form translated values.
   *
   * @param Entity $entity
   *  The entity.
   *
   * @return array
   *  The entity translations and their values.
   */
  public function getTranslatedStorageValues($entity) {
    $langcodes = $this->getLangcodes($entity);
    foreach ($langcodes as $langcode) {
      $translations[$langcode] = $entity->getTranslation($langcode);
    }
    foreach ($translations as $langcode => $translation) {
      $fields = $translation->getTranslatableFields();
      foreach ($fields as $field_name => $field) {
        $values[$langcode][$field_name] = $translation->get($field_name)->getValue();
      }
    }

    return [
      'translations' => $translations,
      'values' => $values
    ];
  }

  /**
   * Get the entity langcodes.
   *
   * @param Entity $entity
   *  The entity.
   *
   * @return array
   *  The translation langcodes.
   */
  public function getLangcodes($entity) {
    $languages = $entity->getTranslationLanguages();
    foreach ($languages as $key => $language) {
      $langcodes[$key] = $language->getId();
    }

    return $langcodes;
  }

  /**
   * Prepares the form before rebuilding it.
   *
   * @param array $form
   *  The entity form render array.
   * @param Entity $entity
   *  The entity attached.
   *
   * @return array
   *  An array of renderable arrays sorted.
   *  - form_elements, the form elements
   *  - translatable, the translatable fields
   *  - all_languages, non translatable fields
   *  - groups, the form groups
   */
  public function sliceForm($form, $entity) {
    $field_definitions = $entity->getFieldDefinitions();
    foreach ($field_definitions as $field_definition) {
      $field_definitions[$field_definition->getName()] = $field_definition;
    }
    // Fetch translatable fields.
    $translatable_fields = $entity->getTranslatableFields();
    $fields_translatable = array_intersect_key($form, $translatable_fields);
    $left_fields = array_diff_key($form, $translatable_fields);
    // Reorganize non translatable fields and groups.
    $non_translatable_fields = array_diff_key($field_definitions, $translatable_fields);
    foreach ($left_fields as $field_name => $field) {
      // Remove non translatable custom fields.
      if (strpos($field_name, 'field_') !== FALSE) {
        $non_editable_fields .= $field_name . ' ';
        continue;
      }
      // Is it a form element?
      if ($field_name[0] == '#') {
        $form_elements[$field_name] = $field;
      }
      else {
        // Is it a non translatable field?
        if (array_key_exists($field_name, $non_translatable_fields)) {
          $fields_all_languages[$field_name] = $field;
        }
        // It's a group.
        else {
          $groups[$field_name] = $field;
        }
      }
    }
    // Move authoring informations to all_languages.
    // @TODO create a function to fecth field based on ['#group'] = 'author',
    // Not to hardcode uid and created.
    $fields_all_languages['uid'] = $fields_translatable['uid'];
    $fields_all_languages['created'] = $fields_translatable['created'];
    unset($fields_translatable['uid']);
    unset($fields_translatable['created']);

    // Set a message for non editable fields.
    if ($non_editable_fields) {
      $this->messenger->addWarning(t('Non translatable field(s) are not shown: @field_list.', [
        '@field_list' => $non_editable_fields,
      ]));
    }

    return [
      'form_elements' => $form_elements,
      'translatable' => $fields_translatable,
      'all_languages' => $fields_all_languages,
      'groups' => $groups,
    ];
  }

   /**
    * Reorganize form groups.
    *
    * @param array $sliced_form
    *  A sliced form array.
    * @param string $langcode
    *  The langcode.
    * @param array $groups
    *  Function callback.
    *
    * @return array
    *   Renderable groups array reorganized by language.
    */
   public function reorderGroups($sliced_form, $langcode, $groups = []) {
     if (!$groups) {
       $groups = $sliced_form['groups'];
     }
     foreach ($groups as $group_name => $group) {
       if (is_array($group) && $group_name[0] != '#') {
         // Iterate over group.
         $groups[$group_name] = $this->reorderGroups($sliced_form, $langcode, $group);
         $changed = FALSE;
         // Apply changes on group.
         foreach ($sliced_form['translatable'] as $field_translatable) {
           if (isset($field_translatable['#group']) && $group_name == $field_translatable['#group'] && $group_name != 'author') {
             // Rename group based on langcode.
             $groups['translatable'][$group_name.'_'.$langcode] = $groups[$group_name];
             $changed = TRUE;
           }
         }
         foreach ($sliced_form['all_languages'] as $field_all_languages) {
           if (isset($field_all_languages['#group']) && $group_name == $field_all_languages['#group']) {
             $groups['all_languages'][$group_name] = $groups[$group_name];
             $changed = TRUE;
           }
         }
         // The group is neither in translatable fields or non translatable fields.
         if (!$changed) {
           $groups['all_languages'][$group_name] = $groups[$group_name];
         }
         unset($groups[$group_name]);
       }
     }

     return $groups;
   }

   /**
    * Apply langcode to fields and groups.
    *
    * @param array $fields
    *   Renderable array of fields.
    * @param string $langcode
    *   The langcode to apply.
    * @param bool $group
    *   True if we work on a group.
    *
    * @return array $fields
    *   The fields localized (with langcode appended to form elements).
    */
   public function localizeFieldGroups($fields, $langcode, $group = FALSE) {
     foreach ($fields as $field_name => $field) {
       if (is_array($field) && $field_name[0] != '#') {
         $fields[$field_name] = $this->localizeFieldGroups($field, $langcode);
         // Localize groups.
         if ($group) {
           if (strpos($field_name, '_'.$langcode) && isset($field['#group']) && $field['#group'] !== 'author') {
             $fields[$field_name]['#group'] = $field['#group'].'_'.$langcode;
           }
         }
         // Localize fields.
         else {
           if (isset($field['#group']) && $field['#group'] !== 'author') {
             $fields[$field_name]['#group'] = $field['#group'].'_'.$langcode;
           }
         }
       }
       if ($field_name === 'path') {
         $fields[$field_name]['widget'][0]['langcode']['#value'] = $langcode;
         $fields[$field_name]['widget'][0]['#attached']['library'] = $this->replaceLibrary($fields[$field_name]['widget'][0]['#attached']['library'], $field_name);
       }
     }

      return $fields;
   }

   /**
    * Apply default values to fields.
    *
    * @param array $fields
    *   Renderable array of localized fields.
    * @param array $values
    *   The form values.
    * @param string $root_field_name
    *   Function callback holding the root field name.
    *  @param string $previous_value
    *   Function callback holding the previous field name.
    *
    * @return array $fields
    *   Renderable array of localized fields with their default values.
    */
  public function setLocalizedDefaultValues($fields, $values, $root_field_name = '', $previous_key = '') {
    $i = 0;
    $keys = array_keys($fields);
    foreach ($fields as $field_name => $field) {
      // At the first level of the array, fetch current and next field names.
      if ($root_field_name === '' || isset($next) && $field_name === $next && $field_name !== 0) {
        $next = isset($keys[$i + 1]) ? $keys[$i + 1] : NULL;
        $root_field_name = $field_name;
      }
      // Walk through the entire mutlidimentional array.
      // The trick resides in passing the root field name along the walk,
      // so we can attach our value to it.
      if (is_array($field) && $field_name[0] != '#') {
        $fields[$field_name] = $this->setLocalizedDefaultValues($field, $values, $root_field_name, $field_name);
      }
      else {
        if ($field_name === '#default_value') {
          // Apply default fields values.
          if ($fields['#type'] === 'entity_autocomplete') {
            $entities = $values[$root_field_name][$fields['#delta']] ? $this->entityTypeManager->getStorage($fields['#target_type'])->loadMultiple($values[$root_field_name][$fields['#delta']]) : [];
            $fields['#default_value'] = is_array($fields['#default_value']) ? $entities : reset($entities);
            unset($values[$root_field_name]);
          }
          foreach ($values[$root_field_name][0] as $value_name => $value) {
            // Target path field and set alias as default.
            if ($root_field_name === 'path' && $value_name === 'alias') {
              $fields['#default_value'] = $value;
            }
            // Target menu.
            if ($root_field_name === 'menu' && $previous_key === $value_name) {
              $fields['#default_value'] = $value;
            }
            // Target all fields which hold one value.
            if (count($values[$root_field_name][0]) == 1) {
              $fields['#default_value'] = $value;
            }
            // Target fields long text with summary.
            elseif (isset($fields['#type']) && $fields['#type'] === 'text_format' && isset($fields['summary'])) {
              $fields['#default_value'] = $values[$root_field_name][0]['value'];
              $fields['#format'] = $values[$root_field_name][0]['format'];
              $fields['summary']['#default_value'] = $values[$root_field_name][0]['summary'];
            }
          }
        }
      }
      // Remove all parents so we use a tree structure.
      if ($field_name === '#parents') {
        unset($fields['#parents']);
      }
      $i++;
    }

  return $fields;
  }

  /**
   * Helper function which produces a one dimentional array
   * holding the field values.
   */
 public function reduceArray(array $array) {
   foreach ($array as $name => $values) {
     if (is_array($values)) {
       foreach ($values as $key => $value) {
         if (is_array($value) && $key !== 'target_id') {
           $array_reduced[$name] = array_reduce($this->reduceArray($value), 'array_merge', []);
         }
         else {
           $array_reduced[$name][$key] = $value;
         }
       }
     }
     else {
       // Make sure every values is nested.
       $array_reduced[$name]['value'] = $values;
     }
   }

   return $array_reduced;
 }

 /**
  * Helper function to replace field attached libraries.
  *
  * Some js in ['#attached'] are not working from the core.
  *
  * @param array $libraries
  *   Content of ['#attached']['library']
  * @param string $field_name
  *   The name of the field.
  */
 public function replaceLibrary($libraries, $field_name) {
   foreach ($libraries as $key => $library) {
     if ($library == $field_name.'/drupal.'.$field_name) {
       $libraries[$key] = 'multilingual_ux/multilingual_ux.'.$field_name;
     }
   }

   return $libraries;
 }

}
