<?php

namespace Drupal\multilingual_ux\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Node routes.
 */
class MultilingualUxNodeController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * Gets a list of node revision IDs for a specific node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node entity.
   * @param \Drupal\node\NodeStorageInterface $node_storage
   *   The node storage handler.
   *
   * @return int[]
   *   Node revision IDs (in descending order).
   */
  public function getTitle(NodeInterface $node) {
    return $this->t('Edit %node_type @title [All translations]', ['%node_type' => $node->type->entity->label(), '@title' => $node->label()]);
  }

}
