/**
 * Custom menu_ui.js
 **/

(function ($, Drupal) {
  Drupal.behaviors.muxMenuUiDetailsSummaries = {
    attach: function attach(context) {
      $(context).find('.menu-link-form').drupalSetSummary(function (context) {
        var $context = $(context);
        if ($context.find('[class*="menu-translate"] input').is(':checked')) {
          return Drupal.checkPlain($context.find('[class*="menu-link-title"] input').val());
        }

        return Drupal.t('Not in menu');
      });
    }
  };
})(jQuery, Drupal);
