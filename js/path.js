/**
 * Custom path.js
 **/

(function ($, Drupal) {
  Drupal.behaviors.muxPathDetailsSummaries = {
    attach: function attach(context) {
      $(context).find('.path-form').drupalSetSummary(function (context) {
        var path = $(context).find('input').val();

        return path ? Drupal.t('Alias: @alias', { '@alias': path }) : Drupal.t('No alias');
      });
    }
  };
})(jQuery, Drupal);
